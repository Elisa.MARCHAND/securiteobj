#!python
import asyncio
from bleak import BleakScanner
from bleak import BleakClient


######################################################
#    Recuperation adresse mac appareil bluetooth     #
######################################################

# async def main():
#     devices = await BleakScanner.discover()
#     for d in devices:
#         print(d)

# asyncio.run(main())

######################################################

######################################################
#             Recuperation info                      #
######################################################

address = "CD:04:92:A2:69:2B"
l = []
fichier = open("data.txt", "wb")


def callback(sender, data):
    print(f"{sender}: {data}")
    if(sender == 58):
        l.append(data)
    if(sender == 66):
        fichier.write(data)
        

async def main(address):
    async with BleakClient(address) as client:
        dico = {}
        service = client.services
        print("\nAffiche les services : \n")
        for s in service:
            print(s)
            for c in s.characteristics: #affichage des caracteristiques 
                print(c)
                dico[c.description] = c.uuid #creation dico
            print("\n")
        
        await client.start_notify(dico.get("ACK"), callback)
        await client.start_notify(dico.get("LOG"), callback)
        await client.start_notify(dico.get("LIST"), callback)
        await client.start_notify(dico.get("READ"), callback)
        
        
        print("\nData dans NUM :")
        number_file = await client.read_gatt_char(dico.get("NUM")) #recupération de la valeur dans NUM
        print(number_file)
        print("\n")

        await client.write_gatt_char(dico.get("LIST"), number_file) #ecriture de la valeur de NUM dans LIST
        
        await asyncio.sleep(5)
        #### boucle pour creer le FileRequeste pour chaque element pas implémenté
        # for elem in l:
        #     print(elem)
        
        #### Pour T10 avec le FileRequeste fait à la main
        ##bytearray(b'T10\x00\x00\x00\x00\x00_\x08\x00\x00n\xe1\x0b\xfe8\xdd\xe8j\xdc\x00\x00\x00')
        # FileRequest = b'T10\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x11'
        # await client.write_gatt_char(dico.get("READ"), FileRequest)
        
        #### Pour T4076 avec le FileRequeste fait à la main
        ## (b'T4076\x00\x00\x00\n\x03\x00\x00\xd3\xeb7\xb4x\xf6\xe9j\xdc\x00\x00\x00'
        FileRequest = b'T4076\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x11'
        await client.write_gatt_char(dico.get("READ"), FileRequest)

        
        await asyncio.sleep(10)
        fichier.close()
        
    

asyncio.run(main(address))

######################################################
